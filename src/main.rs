use fdt::{Fdt, node::FdtNode};

#[cfg(target_os = "redox")] 
fn get_dtb() -> Vec<u8> {
    std::fs::read("kernel/dtb:").unwrap()
}

#[cfg(target_os = "linux")]
fn get_dtb() -> Vec<u8> {
    use std::env;

    if let Some(arg1) = env::args().nth(1) {
        std::fs::read(arg1).unwrap()
    } else {
        Vec::new()
    }
}

fn main() {
    let dtb_data = get_dtb();
    println!("read from OS, len = {}", dtb_data.len());
    if dtb_data.len() != 0 {
        if let Ok(fdt) = Fdt::new(&dtb_data) {
            println!("DTB model = {}", fdt.root().model());
			let mmc = fdt.find_node("/mmc").unwrap();
            print_node(mmc, 0);
            let with = ["brcm,bcm2835-sdhostd"];
            let compat_node = fdt.find_compatible(&with).unwrap();
            print_node(compat_node, 0);
        } else {
            println!("failed to parse dtb_data");
        }
    }
}

fn print_node(node: FdtNode<'_, '_>, n_spaces: usize) {
    (0..n_spaces).for_each(|_| print!(" "));
    println!("node: {}", node.name);

    for child in node.children() {
        print_node(child, n_spaces + 4);
    }
}
